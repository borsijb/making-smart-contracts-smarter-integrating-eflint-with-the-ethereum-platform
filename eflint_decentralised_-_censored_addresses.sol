// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.6.0;

import "https://raw.githubusercontent.com/smartcontractkit/chainlink/master/evm-contracts/src/v0.6/ChainlinkClient.sol";

contract decentralisedEflint is ChainlinkClient {
    
    string public status;
    address[] private oracles;
    uint public decentralisedRequestCounter = 1000;
    uint public decentralisedInstanceCounter = 1000;

    mapping(address => bytes32) public jobIds;
    mapping(bytes32 => uint256) public fees;
    mapping(address => string) public eflintIps;

    mapping(uint => decentralisedRequest) public id2decRequest; //decentralisedRequestId to decentralisedRequest struct
    mapping(bytes32 => uint) public clId2decRequestsID; //Chainlink request ID to decentralisedRequest struct
    mapping(uint => uint) public decRequestID2decInstanceID;

    mapping(uint => decentralisedInstance) decIdToDecInstanceStruct;
    
    
    // Gets transmitted after sending a request
    event RequestSent(
        address indexed from,
        uint decentralisedRequestId
    );
    
    
    // Gets transmitted after request is fulfilled
    event RequestFulfilled(
        uint indexed decentralisedRequestId,
        string requestType,
        string response
    );
        
    
    struct decentralisedRequest{
        uint decentralisedRequestId;
        bool accepted;
        uint numOracles;
        bytes32[] clRequestIds;
        uint numResponses;
        string requestType;
        uint decInstanceID;
        string[] responses;
        string aggregatedResponse;
    }
    
    struct decentralisedInstance {
        mapping(address => bytes32) oracleToUuid;
    }

    /*
     * Function that gets called to deploy the contract.
     */
    constructor() public {
        // status = "created";
        setPublicChainlinkToken();
        oracles = new address[](2);
        oracles[0] = 0x0000000000000000000000000000000000000000; // Chainlink node smart contract address (Censored for code publication)
        oracles[1] = 0x0000000000000000000000000000000000000000; // Chainlink node 2 smart contract address (Censored for code publication)
       
        jobIds[oracles[0]] = "00000000000000000000000000000000"; // Chainlink node POST -> BYTES32 jobID (Censored for code publication)
        jobIds[oracles[1]] = "00000000000000000000000000000000"; // Chainlink node 2 POST -> BYTES32 jobID (Censored for code publication)
       
        eflintIps[oracles[0]] = "0.0.0.0:8888"; // eFLINT web server 1 ip  (Censored for code publication)
        eflintIps[oracles[1]] = "0.0.0.0:8888"; // eFLINT web server 2 ip  (Censored for code publication)

        fees[jobIds[oracles[0]]] = 0.1 * 10 ** 18; // 0.1 LINK;
        fees[jobIds[oracles[1]]] = 0.1 * 10 ** 18; // 0.1 LINK;
    }
    
    
    
    /*
     * Function to send a request for a job to all oracles in the oracle array.
     */
    function sendRequestToOracles(string memory body, string memory path, string memory requestPath, string memory requestType, uint decInstance, string memory parameter) private
    {
        
        uint decRequestID = decentralisedRequestCounter;
        decentralisedRequestCounter += 1;

        id2decRequest[decRequestID].numOracles = oracles.length;
        id2decRequest[decRequestID].decentralisedRequestId = decRequestID;
        id2decRequest[decRequestID].numResponses = 0;
        id2decRequest[decRequestID].decInstanceID = decInstance;
        id2decRequest[decRequestID].requestType = requestType;
        id2decRequest[decRequestID].responses = new string[](oracles.length);
        id2decRequest[decRequestID].clRequestIds = new bytes32[](oracles.length);

        
        if (compareStrings(requestType, "createInstance")){
            uint decInstanceID = decentralisedInstanceCounter;
            decentralisedInstanceCounter += 1;
            decRequestID2decInstanceID[decRequestID] = decInstanceID;
        }
        status = "start";
        
        // Iterate over oracles and send request to each of them
        for(uint i = 0; i < oracles.length; i++) {
            // Setup variables for request
            address oracle = oracles[i];
            bytes32 jobId = jobIds[oracle];
            uint256 fee = fees[jobId];
            string memory ip = eflintIps[oracle];
            string memory url = string(abi.encodePacked("http://", ip, "/", requestPath));
            
            //Building body for each case:
            if(compareStrings(requestType, "command")){
                bytes32 uuid = decIdToDecInstanceStruct[decInstance].oracleToUuid[oracle];
                body = string(abi.encodePacked("{  \"uuid\": \"", uuid, "\",\"request-type\": \"command\", \"data\":{ \"command\":\"", parameter,"\"}}"));
            }
            else if(compareStrings(requestType, "phrase")){
                bytes32 uuid = decIdToDecInstanceStruct[decInstance].oracleToUuid[oracle];
                body = string(abi.encodePacked("{  \"uuid\": \"", uuid, "\", \"request-type\": \"command\", \"data\":{ \"command\": \"phrase\", \"text\": \"", parameter, "\"}}"));
            }
            else if(compareStrings(requestType, "revert")){
                bytes32 uuid = decIdToDecInstanceStruct[decInstance].oracleToUuid[oracle];
                body = string(abi.encodePacked("{  \"uuid\": \"", uuid, "\", \"request-type\": \"command\", \"data\":{ \"command\": \"revert\", \"value\": ", parameter, "}}"));
            }
            else if(compareStrings(requestType, "requestViolations")){
                bytes32 uuid = decIdToDecInstanceStruct[decInstance].oracleToUuid[oracle];
                body = string(abi.encodePacked("{  \"uuid\": \"", uuid, "\", \"request-type\": \"command\", \"data\":{ \"command\": \"status\"}}"));
            }

            
            // Building chainlink request
            Chainlink.Request memory request = buildChainlinkRequest(jobId, address(this), this.fulfill.selector);
            request.add("post", url);
            request.add("path", path);
            request.add("body", body);
            

            // Send chainlink request, write data to decentralisedRequestId struct.
            bytes32 oracleRequestId = sendChainlinkRequestTo(oracle, request, fee);
            clId2decRequestsID[oracleRequestId] = decRequestID; 

        }
        emit RequestSent(msg.sender, decRequestID);

    }
    

    /*
     * Uses chainlink to call eflint server to create an instance from a template.
     * Returns chainlink request ID.
     */
    function createInstanceFromTemplate(string memory templateName) public
    {
        status = "start request for instance creation from template";
        string memory pathToTemplate = string(abi.encodePacked("./src/main/resources/templates/", templateName, ".eflint"));
        string memory body = string(abi.encodePacked("{\"template-name\": \"", pathToTemplate, "\" ,\"values\" :{}, \"flint-search-paths\" : [] }"));
        string memory path = "data.uuid";
        sendRequestToOracles(body, path, "create", "createInstance", 0, "");
    }

    /*
     * Uses chainlink to call eflint server to create an instance from a template.
     * Returns chainlink request ID.
     */
    function createInstanceFromExample(string memory exampleName) public
    {
        status = "start request for instance creation from example";
        string memory pathToExample = string(abi.encodePacked("./src/main/resources/examples/", exampleName, ".eflint"));
        string memory body = string(abi.encodePacked("{\"template-name\": \"", pathToExample, "\" ,\"values\" : {}, \"flint-search-paths\" : [] }"));
        string memory path = "data.uuid";
        sendRequestToOracles(body, path, "create", "createInstance", 0, "");
    }
    
    /*
     * Uses chainlink to call eflint server to send a phrase to an eFLINT instance.
     */
    function sendPhrase(uint decentralisedInstanceID, string memory phrase) public
    {
        status = "start request for sending phrase command";
        string memory path = "data.response.response";
        sendRequestToOracles("", path, "command", "phrase", decentralisedInstanceID, phrase);
    }
    
    /*
     * Uses chainlink to call eflint server to check if the given duty was violated.
     */
    function checkDutyViolated(uint decentralisedInstanceID, string memory duty) public
    {       
        string memory fullPhrase = string(abi.encodePacked("?Violated(", duty));
        sendPhrase(decentralisedInstanceID, fullPhrase);
    }
    
    
    /* 
     * Reverts the state of the eFLINT instance n steps. Choose negative n to revert to start.
     */
    function revertState(uint decentralisedInstanceID, int n) public
    {
        status = "start request for revert command";
        string memory path = "data.response.response";
        sendRequestToOracles("", path, "command", "revert", decentralisedInstanceID, int2str(n));
        
    }
    
    /*
     * Uses chainlink to call eflint server to check for violations of an instance. 
     * Emits event with the chainlink request ID. 
     */
    function requestViolations(uint decentralisedInstanceID) public
    {
        status = "start request for violations";
        string memory path = "data.response.violations";
        sendRequestToOracles("", path, "command", "requestViolations", decentralisedInstanceID, "");
    }


    
     /*
      * Uses chainlink to call eflint server to kill an eflint instance.
      * Emits event with the chainlink request ID.
      */
    function killInstance(uint decentralisedInstanceID) public
    {
        status = "start request for kill";
        string memory path = "data.response";
        sendRequestToOracles("", path, "command", "command", decentralisedInstanceID, "kill");
    }   
    
    /*
     * Kills all running eFLINT instances
     */
    function killAll() public{
        status = "start request for kill_all";

        string memory path = "status";
        sendRequestToOracles("", path, "kill_all", "kill_all", 0, "kill_all");
    }

    /*
     * Check if two strings are identical. Source: https://ethereum.stackexchange.com/questions/30912/how-to-compare-strings-in-solidity/82739
     */
    function compareStrings(string memory a, string memory b) public pure returns (bool) {
        return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }
    
    /*
     * Converts a bytes32 to a string. Source: https://ethereum.stackexchange.com/questions/2519/how-to-convert-a-bytes32-to-string/2834
     */
    function bytes32ToString(bytes32 _bytes32) public pure returns (string memory) {
        uint8 i = 0;
        while(i < 32 && _bytes32[i] != 0) {
            i++;
        }
        bytes memory bytesArray = new bytes(i);
        for (i = 0; i < 32 && _bytes32[i] != 0; i++) {
            bytesArray[i] = _bytes32[i];
        }
        return string(bytesArray);
    }
    
    /*
     * Converts an int to a string. Based on: https://stackoverflow.com/questions/47129173/how-to-convert-uint-to-string-in-solidity
     */
    function int2str(int _input) public pure returns (string memory _uintAsString) {
        uint _i;
        if(_input < 0){
            _i = uint(0 - _input);
        } else {
            _i = uint(_input);
        }
        
        if (_i == 0) {
            return "0";
        }
        uint j = _i;
        uint len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint k = len;
        while (_i != 0) {
            k = k-1;
            uint8 temp = (48 + uint8(_i - _i / 10 * 10));
            bytes1 b1 = bytes1(temp);
            bstr[k] = b1;
            _i /= 10;
        }


        if(_input < 0){
            return string(abi.encodePacked("-", string(bstr)));
        } else {
            string(bstr);
        }
    }
    
    /*
     * Converts an int to a string. Based on: https://stackoverflow.com/questions/47129173/how-to-convert-uint-to-string-in-solidity
     */
    function uint2str(uint _i) internal pure returns (string memory _uintAsString) {
        uint i = _i;
        if (i == 0) {
            return "0";
        }
        uint j = i;
        uint len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint k = len;
        while (i != 0) {
            k = k-1;
            uint8 temp = (48 + uint8(i - i / 10 * 10));
            bytes1 b1 = bytes1(temp);
            bstr[k] = b1;
            i /= 10;
        }
        return string(bstr);
    }
    
    
    
    /*
     * This function gets called by the chainlink node which fulfills the request
     */ 
    function fulfill(bytes32 _requestId, bytes32 _response) public recordChainlinkFulfillment(_requestId)
    {
        status = "response";
        uint request = clId2decRequestsID[_requestId];
        id2decRequest[request].responses[id2decRequest[request].numResponses] = bytes32ToString(_response);
        id2decRequest[request].numResponses += 1;
        

        // If the requestType is createInstance, each server will respond with the uuid of the created eFLINT instance.
        // The "aggregated response", that will be returned will be a decentralisedInstanceID.
        if (compareStrings(id2decRequest[request].requestType, "createInstance")){
            address sourceOracle = msg.sender;
            decIdToDecInstanceStruct[decRequestID2decInstanceID[request]].oracleToUuid[sourceOracle] = _response;
            id2decRequest[request].aggregatedResponse = uint2str(decRequestID2decInstanceID[request]);
            
        }
        // If every oracle has responded. Check if their responses are the same.
        else if (id2decRequest[request].numResponses == id2decRequest[request].numOracles){
            
            string memory firstResponse = id2decRequest[request].responses[0];
            id2decRequest[request].accepted = true;  //Asnwer is accepted, until proven otherwise
            
            // Check if all responses are the same as the first response.
            for(uint i = 1; i < id2decRequest[request].numOracles; i++) {
                if (! compareStrings(firstResponse, id2decRequest[request].responses[i])) {
                    id2decRequest[request].accepted = false;
                }
            }
            
            if (id2decRequest[request].accepted){
                id2decRequest[request].aggregatedResponse = bytes32ToString(_response);
                emit RequestFulfilled(id2decRequest[request].decentralisedRequestId, id2decRequest[request].requestType, bytes32ToString(_response));
            }
            


        }
    }


}